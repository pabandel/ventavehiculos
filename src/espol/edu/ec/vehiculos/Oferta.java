/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.vehiculos;

import java.util.Objects;

/**
 *
 * @author miguel
 */
public class Oferta implements Comparable{
    
    private String placa;
    private String correo;
    private double oferta;

    public Oferta(String placa, String correo, double oferta) {
        this.placa = placa;
        this.correo = correo;
        this.oferta = oferta;
    }
    
    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public double getOferta() {
        return oferta;
    }

    public void setOferta(double oferta) {
        this.oferta = oferta;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }
    
    
    
    @Override
    public String toString() {
        return placa + "|" + correo + "|" + oferta;
    }

    @Override
    public int compareTo(Object t) {
        if(t instanceof Oferta){
            Oferta of = (Oferta) t;
            if(this.oferta > of.getOferta()) return 1;
            if(this.oferta < of.getOferta()) return -1;
        }
        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        final Oferta other = (Oferta) obj;
        if (Double.doubleToLongBits(this.oferta) != Double.doubleToLongBits(other.oferta)) {
            return false;
        }
        if (!Objects.equals(this.placa, other.placa)) {
            return false;
        }
        if (!Objects.equals(this.correo, other.correo)) {
            return false;
        }
        return true;
    }
    
    
    
    
}
