
package espol.edu.ec.vehiculos;

import javafx.scene.image.Image;

/**
 *
 * @author Pablo Delgado
 */
public class Vehiculo {
    
    private String placa;
    private String marca;
    private String modelo;
    private float tipoMotor;
    private int anio;
    private String tipoVehiculo;
    private double recorrido;
    private String color;
    private String tipoCombustible;
    private String vidrios;
    private String trasmision;
    private double precio;
    private String imagen;

    public Vehiculo(String placa, String marca, String modelo, float tipoMotor, int anio, String tipoVehiculo, 
                    double recorrido, String color, String tipoCombustible, String vidrios, String trasmision, 
                    double precio, String imagen) {
        this.placa = placa;
        this.marca = marca;
        this.modelo = modelo;
        this.tipoMotor = tipoMotor;
        this.anio = anio;
        this.tipoVehiculo = tipoVehiculo;
        this.recorrido = recorrido;
        this.color = color;
        this.tipoCombustible = tipoCombustible;
        this.vidrios = vidrios;
        this.trasmision = trasmision;
        this.precio = precio;
        this.imagen = imagen;
    }
    
    

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public double getRecorrido() {
        return recorrido;
    }

    public void setRecorrido(double recorrido) {
        this.recorrido = recorrido;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getVidrios() {
        return vidrios;
    }

    public void setVidrios(String vidrios) {
        this.vidrios = vidrios;
    }

    public String getTrasmision() {
        return trasmision;
    }

    public void setTrasmision(String trasmision) {
        this.trasmision = trasmision;
    }
    
    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }  

    public float getTipoMotor() {
        return tipoMotor;
    }

    public void setTipoMotor(float tipoMotor) {
        this.tipoMotor = tipoMotor;
    }

    public String getTipoVehiculo() {
        return tipoVehiculo;
    }

    public void setTipoVehiculo(String tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    public String getTipoCombustible() {
        return tipoCombustible;
    }

    public void setTipoCombustible(String tipoCombustible) {
        this.tipoCombustible = tipoCombustible;
    }

    @Override
    public String toString() {
        return placa + "|" + marca + "|" + modelo + "|" + tipoMotor + "|" + anio + "|" 
               + tipoVehiculo + "|" + recorrido + "|" + color + "|" + tipoCombustible 
                + "|" + vidrios + "|" + trasmision + "|" + precio + "|" + imagen;
    }
    
    @Override
    public boolean equals(Object o){
        if (o instanceof Vehiculo){
            return this.placa.equals(((Vehiculo) o).placa);
        }
        return false;
    }
    
    public boolean exactEquals(Vehiculo v){
        return marca.equals(v.marca) && modelo.equals(v.modelo) && tipoMotor == v.tipoMotor && anio == v.anio && tipoVehiculo.equals(v.tipoVehiculo)
                && recorrido == v.recorrido && color.equals(v.color) && tipoCombustible.equals(v.tipoCombustible) && vidrios.equals(v.vidrios) 
                && trasmision.equals(v.trasmision) && precio == v.precio && imagen.equals(v.imagen);
    }
    
}
