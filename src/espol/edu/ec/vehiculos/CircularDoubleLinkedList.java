/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package espol.edu.ec.vehiculos;

/**
 *
 * @author Pablo Delgado
 */
public class CircularDoubleLinkedList<E> implements List<E> {
    
    private Nodo<E> first, last;
    private int efectivo;
    
    public CircularDoubleLinkedList(){
        this.first = this.last = null;
        this.efectivo = 0;
    }

    @Override
    public boolean isEmpty() {
        return this.first == null && this.last == null;
    }

    @Override
    public int size() {
        return efectivo;
    }

    @Override
    public boolean addFirst(E element) {
        Nodo<E> nodo = new Nodo<>(element);
        if (element == null)
            return false;
        else if (this.isEmpty())
            this.first = this.last = nodo;
        else{
            nodo.setNext(this.first);
            this.first.setPrevious(nodo);
            this.first = nodo;
        }this.efectivo++;
        this.first.setPrevious(this.last);
        this.last.setNext(this.first);
        return true;
    }

    @Override
    public boolean addLast(E element) {
        Nodo<E> nodo = new Nodo<>(element);
        if (element == null)
            return false;
        else if (this.isEmpty())
            this.first = this.last = nodo;
        else{
            this.last.setNext(nodo);
            nodo.setPrevious(this.last);
            this.last = nodo;
        }this.efectivo++;
        this.last.setNext(this.first);
        this.first.setPrevious(this.last);
        return true;
    }

    @Override
    public E getFirst() {
        if (!this.isEmpty())
            return this.first.getData();
        return null;
    }

    @Override
    public E getLast() {
        if (!this.isEmpty())
            return this.last.getData();
        return null;
    }

    @Override
    public boolean removeFirst() {
        if (this.isEmpty())
            return false;
        else if(this.first == this.last)
            this.first = this.last = null;
        else{
            Nodo<E> temp = this.first.getNext();
            this.first.setNext(null);
            this.first = temp;
            temp.setPrevious(this.last);
            this.last.setNext(temp);
        }this.efectivo--;
        return true;
    }

    @Override
    public boolean removeLast() {
        if (this.isEmpty())
            return false;
        else if (this.first == this.last)
            this.first = this.last = null;
        else{
            Nodo<E> temp = this.last.getPrevious();
            this.last.setPrevious(null);
            this.last = temp;
            temp.setNext(this.first);
            this.first.setPrevious(temp);
        }this.efectivo--;
        return true;
    }

    @Override
    public boolean contanins(E element) {
        this.first.setPrevious(this.last);
        this.last.setNext(this.first);
        Nodo<E> primero = this.first, ultimo = this.last;
        do{
            if (ultimo.getData().equals(element) || primero.getData().equals(element))
                return true;
            primero = primero.getNext();
            ultimo = ultimo.getPrevious();
        }while(ultimo.getNext()!= primero && primero != ultimo);
        return false;
    }

    @Override
    public E get(int index) {
        this.last.setNext(this.first);
        this.first.setPrevious(this.last);
        int indice1 = 0;
        int indice2 = this.efectivo-1;
        Nodo<E> antes = this.first, despues =this.last;
        if (!this.isEmpty() && indice1 >= 0 && indice1 < this.efectivo){
            do{
                if (indice1 == index)
                    return antes.getData();
                else if (indice2 == index)
                    return despues.getData();
                indice1++;
                indice2--;
                antes = antes.getNext();
                despues = despues.getPrevious();
            }while(indice1 <= indice2);
        }return null;
    }

    @Override
    public List<E> slicing(int inicio, int fin) {
        CircularDoubleLinkedList<E> list = new CircularDoubleLinkedList<>();
        int indice = 0;
        this.first.setPrevious(this.last);
        Nodo<E> temp = this.first;
        this.last.setNext(this.first);
        do{
            if (indice == this.efectivo-1 && inicio > fin){//Puede que el inicio sea el o los últimos indices
                indice = 0;
                list.addLast(temp.getData());
            }else if (indice >= inicio && indice <= fin)
                list.addLast(temp.getData());
            temp = temp.getNext();
            indice++;
        }while(temp.getNext() != this.first);
        return list;
    }

    @Override
    public boolean remove(int index) {
        if (index==0 && !isEmpty())
            this.removeFirst();
        else if (index == this.efectivo-1)
            this.removeLast();
        else if(!isEmpty() && index >=0 && index < this.efectivo){
            int indice = 0;
            this.last.setNext(this.first);
            this.first.setPrevious(this.last);
            Nodo<E> siguiente, anterior;
            Nodo<E> nodo = this.first;
            do{
                if (indice == index){
                    siguiente = nodo.getNext();
                    anterior = nodo.getPrevious();
                    nodo.setNext(null);
                    nodo.setPrevious(null);
                    siguiente.setPrevious(anterior);
                    anterior.setNext(siguiente);
                    this.efectivo--;
                    return true;
                }indice++;
                nodo = nodo.getNext();
            }while(indice <= index);
        }return false;
    }

    @Override
    public E set(int index, E element) {
        this.last.setNext(this.first);
        this.first.setPrevious(this.last);
        Nodo<E> elemento = new Nodo<>(element);
        if (index == 0 && !this.isEmpty())
            return elemento.getData();
        else if (!this.isEmpty() && index >=0 && index < this.efectivo){
            int indice =0;
            Nodo<E> temp = this.first;
            do{
                if (indice == index){
                    temp.setData(element);
                    return temp.getData();
                }indice++;
                temp = temp.getNext();
            }while(temp.getNext() != this.first);
        }return null;
    }

    @Override
    public boolean add(int index, E element) {
        this.last.setNext(this.first);
        this.first.setPrevious(this.last);
        if (index >=0 && index < this.efectivo){
            if (index == 0 && !this.isEmpty())
                this.addFirst(element);
            else if (index == this.efectivo-1)
                this.addLast(element);
            else{
                int indice = 0;
                Nodo<E> nodo = new Nodo<>(element);
                Nodo<E> temp = this.first;
                do{
                    if(indice == index){
                        Nodo<E> anterior = temp.getPrevious();
                        nodo.setNext(temp);
                        nodo.setPrevious(anterior);
                        temp.setPrevious(nodo);
                        anterior.setNext(nodo);
                        this.efectivo++;
                        return true;
                    }indice++;
                    temp = temp.getNext();
                }while(temp.getNext() != this.first);
            }
        }return false;
    }

    @Override
    public int indexOf(E element) {
        this.first.setPrevious(this.last);
        this.last.setNext(this.first);
        if (!this.isEmpty()){
            int indice1= 0;
            int indice2= this.efectivo-1;
            Nodo<E> primero = this.first, ultimo = this.last;
            do{
                if (primero.getData().equals(element))
                    return indice1;
                else if (ultimo.getData().equals(element))
                    return indice2;
                indice1++;
                indice2--;
                primero = primero.getNext();
                ultimo = ultimo.getPrevious();
            }while(ultimo.getNext()!= primero && primero != ultimo);
        }return -1;
    }
    
    @Override
    public String toString() {
        String cad = "";
        Nodo<E> temp = this.first;
        this.last.setNext(this.first);
        this.first.setPrevious(this.last);
        do{
            if (temp == this.last)
                cad = cad+temp.getData();
            else 
                cad = cad+temp.getData()+",";
            temp = temp.getNext();
        }while(temp != this.first);
        return "["+cad+"]";
    }
    
    public E getNext(){
        Nodo<E> temp = this.first;
        
        return this.first.getData();
    }
    
}
