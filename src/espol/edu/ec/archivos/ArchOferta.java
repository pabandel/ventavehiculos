package espol.edu.ec.archivos;

import espol.edu.ec.vehiculos.Oferta;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author Pablo Delgado
 */
public class ArchOferta {
    
    public void escribirOfertas(LinkedList<Oferta> ofertaVehiculo){
        PrintWriter file = null;
        try{
            file = new PrintWriter(new FileWriter("src/espol/edu/ec/archivos/ofertas.txt"));
            for (Oferta oferta :ofertaVehiculo){
                file.write(oferta.toString()+"\n");
            }
            file.close();
        }catch(Exception e){
//            JOptionPane.showInternalMessageDialog(null, e);
            System.err.println(e);
        }
    }
    
    public LinkedList<Oferta> leerOfertas(){
        LinkedList<Oferta> lista = new LinkedList<>();
        try{
            Scanner scan = new Scanner(new File("src/espol/edu/ec/archivos/ofertas.txt"));
            while (scan.hasNext()){
                String[] linea = scan.nextLine().split("\\|");
                Oferta o = new Oferta(linea[0],linea[1],Double.parseDouble(linea[2]));
                lista.add(o);
            }
        }catch(Exception e){
//            JOptionPane.showInternalMessageDialog(null, e);
            System.err.println(e);
        }
        return lista;
    }
    
}
