
package espol.edu.ec.archivos;

import espol.edu.ec.vehiculos.Vehiculo;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Scanner;
import javax.swing.JOptionPane;
/**
 *
 * @author Pablo Delgado
 */
public class ArchVehiculo {
    
    public ArchVehiculo(){}
    
    
    public void escribirVehiculos(LinkedList<Vehiculo> vehiculos){
        PrintWriter file = null;
        try{
            file = new PrintWriter(new FileWriter("src/espol/edu/ec/archivos/autos.txt"));
            for (Vehiculo vehiculo :vehiculos){
                file.write(vehiculo.toString()+"\n");
            }
            file.close();
        }catch(Exception e){
//            JOptionPane.showInternalMessageDialog(null, e);
            System.err.println(e);
        }
    }
    
    public LinkedList<Vehiculo> leerVehiculo(){
        LinkedList<Vehiculo> lista = new LinkedList<>();
        try{
            Scanner scan = new Scanner(new File("src/espol/edu/ec/archivos/autos.txt"));
            while (scan.hasNext()){
                String[] linea = scan.nextLine().split("\\|");
                Vehiculo v = new Vehiculo(linea[0],linea[1],linea[2],Float.parseFloat(linea[3]),
                        Integer.parseInt(linea[4]),linea[5],Double.parseDouble(linea[6]),linea[7],
                        linea[8],linea[9],linea[10],Double.parseDouble(linea[11]),linea[12]);
                lista.add(v);
            }
        }catch(Exception e){
//            JOptionPane.showInternalMessageDialog(null, e);
            System.err.println(e);
        }
        return lista;
    }
    
}
