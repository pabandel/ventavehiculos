
package espol.edu.ec.principal;

import java.io.File;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author pablo
 */
public class FrmInicio extends javax.swing.JFrame {

    /**
     * Creates new form FrmInicio
     */
    public FrmInicio() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtUsuario = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtContrasenia = new javax.swing.JPasswordField();
        Ingresar = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMaximizedBounds(new java.awt.Rectangle(650, 650, 650, 650));
        setMinimumSize(new java.awt.Dimension(750, 450));
        getContentPane().setLayout(null);

        txtUsuario.setName("txtUsuario"); // NOI18N
        getContentPane().add(txtUsuario);
        txtUsuario.setBounds(230, 100, 222, 34);

        jLabel1.setBackground(new java.awt.Color(0, 0, 0));
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Usuario");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(147, 110, 70, 15);

        jLabel2.setBackground(new java.awt.Color(0, 0, 0));
        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setText("Contrasena");
        jLabel2.setName(""); // NOI18N
        getContentPane().add(jLabel2);
        jLabel2.setBounds(136, 170, 80, 15);

        txtContrasenia.setToolTipText("");
        txtContrasenia.setName("txtContrasenia"); // NOI18N
        getContentPane().add(txtContrasenia);
        txtContrasenia.setBounds(230, 160, 222, 37);

        Ingresar.setText("Ingresar");
        Ingresar.setName("btnIngresar"); // NOI18N
        Ingresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IngresarActionPerformed(evt);
            }
        });
        getContentPane().add(Ingresar);
        Ingresar.setBounds(310, 220, 74, 25);
        Ingresar.getAccessibleContext().setAccessibleDescription("");

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/espol/edu/ec/archivos/Imagenes/fondo1.png"))); // NOI18N
        jLabel4.setAlignmentX(500.0F);
        jLabel4.setAlignmentY(500.0F);
        getContentPane().add(jLabel4);
        jLabel4.setBounds(0, 0, 740, 400);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void IngresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IngresarActionPerformed
        // TODO add your handling code here:
        try {
            boolean tieneAcceso = false;
            String rol = "";
            Scanner scanner = new Scanner(new File("src/espol/edu/ec/archivos/usuarios.txt"));
            while (scanner.hasNext()){
                String[] linea = scanner.nextLine().split("\\|");
                if (linea[0].equals(txtUsuario.getText()) && 
                        linea[1].equals(String.valueOf(txtContrasenia.getPassword()))) {
                    rol = linea[2];
                    tieneAcceso = true;
                    break;
                }
            }
            if (tieneAcceso){
                if (rol.equals("v")){
                    FrmVendedor frmMenu = new FrmVendedor();
                    frmMenu.setVisible(true);
                    this.setVisible(false);
                }else{
                    FrmComprador frmMenu2 = new FrmComprador();
                    frmMenu2.setVisible(true);
                    this.setVisible(false);
                }                
            }
            else 
                JOptionPane.showMessageDialog(null, "Error en las credenciales", "Mensaje del sistema", JOptionPane.ERROR_MESSAGE);
        } catch (Exception ex) {
            System.err.println(ex);
        }
    }//GEN-LAST:event_IngresarActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmInicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmInicio().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Ingresar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPasswordField txtContrasenia;
    private javax.swing.JTextField txtUsuario;
    // End of variables declaration//GEN-END:variables
}
